import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';

import { saga } from './apps';
import { history, reducerRegistry, combine } from './services';
import ui from './apps/ui';
import authentication from './apps/authentication';

//create saga-middleware
const sagaMiddleWare = createSagaMiddleware();

//register static reducers first
reducerRegistry.register(ui.constants.NAME, ui.reducer);
reducerRegistry.register('router', connectRouter(history));
reducerRegistry.register(authentication.constants.NAME, authentication.reducer);

//build root reducer first with static reducers only
const reducer = combine(reducerRegistry.getReducers());

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

//Load initialState from localStorage
const initialState = localStorage['data'] ? JSON.parse(localStorage['data']) : {};

let store = createStore(
    reducer,
    initialState,
    composeEnhancers(
        applyMiddleware(
            routerMiddleware(history),
            sagaMiddleWare
        ),
    )
);

//on each dispatch, save state to localStorage
store.subscribe(() => { localStorage['data']=JSON.stringify(store.getState()) })

//register new reducer based on code-splitting
reducerRegistry.setChangeListener(reducers => {
    store.replaceReducer(combine(reducers));
});

sagaMiddleWare.run(saga);

export default store;