/**
 * CustomTable Component.
 * 
 * InputProps: See the PropTypes at the bottom.
 */

import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import {
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Button,
    TextField,
    IconButton,
    withStyles,
    Toolbar,
} from '@material-ui/core';
import {
    ArrowDownward as DescIcon,
    ArrowUpward as AscIcon,
    Sort as SortIcon,
} from '@material-ui/icons';

import CustomCell from './custom-cell';

import styles from './styles';

/**
 * DISCUSS: CustomTable composition example. 
 */

class CustomTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            _rows: [],
            rows: [],
            columns: [],
            filterCol: [],
            editCol: [], 
            sortCol: [], 
            addFilter: false,
            chosenSortCol: null, 
            searchText: '', 
            filterArgs: {},
        };
    }

    setUp = (rows, columns) => {
        let filterCol = [];
        let editCol = [];
        let sortCol = [];
    
        // recheck for deep and shallow copies (_rows keeps original copy to restore after filtering and searching)
        let _rows = rows;
        let filterArgs = {};
    
        columns.map((col,colInd) => {
            const { filter, edit, sort } = col;
            filterCol.push(filter==true);
            editCol.push(edit==true);
            sortCol.push(sort==true);
            filterArgs[colInd] = '';
        });
    
        this.setState({ _rows, rows, columns, filterCol, editCol, sortCol, addFilter: false, chosenSortCol: null, searchText: '', filterArgs });
    }

    shouldComponentUpdate({ rows: newPropRows, columns }, { rows: newStateRows }) {
        const { rows: currentPropRows } = this.props;
        const { rows: currentStateRows } = this.state;

        if (newPropRows != currentPropRows) {
            this.setUp(newPropRows, columns);
            return true;
        }
        else if (newStateRows != currentStateRows) 
            return true;
        else
            return false;
    }

    componentDidMount() {
        const { rows, columns } = this.props;
        this.setUp(rows, columns);
    }

    updateCell = (row, cell, text) => {
        // since it is a shallow copy, both _rows and row gets updated
        let { rows } = this.state;
        rows[row].data[cell] = text.trim();
        this.setState({ rows });
    }

    match = (type='', first, second) => {
        if (type != 'string')
            first = first.toString();
        return (first.toLowerCase().includes(second.toLowerCase()) ||
            second=='');
    }

    performFilter = (searchText, filterArgs) => {
        // filters a particular column
        const { columns } = this.state;
        let { _rows: rows } = this.state;

        let totalCols = columns.length;

        // apply filters for all columns
        for (let i=0;i<totalCols;i++) {
            let tmp = [];            
            rows.map(row => this.match(columns[i].type, row.data[i], filterArgs[i]) && tmp.push(row));
            rows = tmp;
        }

        // perform search
        let tmp = [];
        rows.map(row => {
            let found = false;
            row.data.map((col, index) => { found = found || this.match(columns[index].type, col, searchText); });

            if (found)
                tmp.push(row);
        });

        this.setState({ rows: tmp, searchText, filterArgs });
    }

    cmp = (order, first, second) => {
        const diff = (first < second) ? -1: 1;
        return (order == 'asc') ? diff: -diff;
    }

    sort = (col) => {
        let { rows, chosenSortCol } = this.state;

        if (chosenSortCol==null)
            chosenSortCol = { col, order: 'asc' };
        else {
            if (chosenSortCol.col != col)
                chosenSortCol = { col, order: 'asc' };
            else
                chosenSortCol = { col, order: (chosenSortCol.order=='asc' ? 'desc': 'asc') };
        }

        rows.sort((a, b) => this.cmp(chosenSortCol.order,a.data[col],b.data[col]));
        this.setState({ chosenSortCol, rows });
    }

    render() {
        const { rows, columns, editCol, filterCol, addFilter, chosenSortCol } = this.state;
        const { classes, indexing } = this.props;
        let { filterArgs, searchText } = this.state;
        
        return (
            <div className={classes.wrapper}>

                <Toolbar className={classes.toolsContainer} >
                    <Button className={classes.csvButton} onClick={() => console.log("excel")}>EXCEL</Button>
                    <Button className={classes.pdfButton} onClick={() => console.log("pdf")}>PDF</Button>
                    <TextField variant="outlined" placeholder="Search" onChange={(event) => this.performFilter(event.target.value, filterArgs)} />
                    {filterCol.some(any => any==true) && 
                        <Button className={classes.filterButton} onClick={() => {
                                                    columns.map((col,colInd) => { filterArgs[colInd]='' });
                                                    this.setState({ addFilter: !addFilter, filterArgs });
                                                    this.performFilter(searchText, filterArgs);
                                                }}>
                            {addFilter ? `Remove Filter` : `Filter` }
                        </Button>
                    }
                </Toolbar>


                <Table className={classes.table}>
                    <TableHead className={classes.tableHead}>
                        <TableRow>
                            {indexing==true && <TableCell variant="head">#</TableCell>}
                            {columns.map((col,colInd) =>
                                <Fragment key={colInd}>
                                    <TableCell variant="head">
                                        {col.name}
                                        {col.sort && 
                                            <IconButton className={classes.sortIconButton} onClick={() => this.sort(colInd)}>
                                                {(chosenSortCol==null || chosenSortCol.col!=colInd) && <SortIcon/>}
                                                {(chosenSortCol!=null && chosenSortCol.col==colInd && ((chosenSortCol.order=='asc') ? <AscIcon /> : <DescIcon />))}
                                            </IconButton>
                                        }
                                    </TableCell>
                                </Fragment>
                            )}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {addFilter && 
                            <TableRow className={classes.tableRow}>
                                {indexing && <TableCell></TableCell>}
                                {filterCol.map((each,col) => 
                                    <TableCell key={col}>
                                        {each ? 
                                            <TextField 
                                                variant="outlined"
                                                placeholder={columns[col].name} 
                                                onChange={(event) => this.performFilter(searchText, { ...filterArgs, [col]: event.target.value})} />
                                            :
                                            <Fragment />
                                        }
                                    </TableCell>
                                )}
                            </TableRow>
                        }
                        {rows.map((row,rowInd) => 
                            (row.link==undefined) ?
                                <TableRow key={rowInd}>
                                    {indexing && <TableCell>{rowInd + 1}</TableCell>}
                                    {row.data.map((cell,cellInd) => 
                                        <CustomCell
                                            canEdit={editCol[cellInd]}
                                            key={cellInd}
                                            rowInd={rowInd}
                                            cellInd={cellInd}
                                            updateCell={this.updateCell}
                                        >
                                            {cell}
                                        </CustomCell>
                                    )}
                                </TableRow>
                                :
                                <TableRow 
                                    className={classes.rowLink} 
                                    key={rowInd} 
                                    onClick={() => this.props.history.push(row.link) }>
                                    {indexing==true && <TableCell>{rowInd + 1}</TableCell>}
                                    {row.data.map((cell,cellInd) => 
                                        <CustomCell
                                            style={{ color: 'inherit' }}
                                            canEdit={editCol[cellInd]}
                                            key={cellInd}
                                            rowInd={rowInd}
                                            cellInd={cellInd}
                                            updateCell={this.updateCell}
                                        >
                                            {cell}
                                        </CustomCell>
                                    )}
                                </TableRow>
                        )}
                    </TableBody>
                </Table>
            </div>
        )
    }
}

CustomTable.propTypes = {
    /**
     * eg. columns: [
     *      { name: 'ID', type: 'number', filter: true },
     *      { name: 'Name', type: 'string', edit: true, filter: true },
     *     ]
     */
    columns: PropTypes.arrayOf(PropTypes.shape({
        /* TODO: Add explanation. */
        type: PropTypes.string.isRequired,

        /**
         * name value is used in table header.
         */
        name: PropTypes.string.isRequired,

        /**
         * The sort value dictates whether the table data can be filtered on the particular column or not.  
         */
        edit: PropTypes.bool,

        /**
         * The sort value dictates whether the table data can be filtered on the particular column or not.  
         */
        filter: PropTypes.bool,

        /**
         * The sort value dictates whether the table data can be sorted on the particular column or not.  
         */
        sort: PropTypes.bool,
    })).isRequired,

    rows: PropTypes.arrayOf(PropTypes.shape({
        link: PropTypes.string,
        /**
         * The size of data array must be the same as the size of columns array(CustomTable Prop). 
         */
        data: PropTypes.array.isRequired,
    })).isRequired,

    indexing: PropTypes.bool,
}

export default withRouter(withStyles(styles)(CustomTable));