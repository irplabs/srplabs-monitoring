import { red, green, blue } from '@material-ui/core/colors';

export default (theme) => ({
    wrapper: {
        
    },
    toolsContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        overflowX: 'auto',

        '& > *': {
            margin: theme.spacing.unit,
        },
    },
    rowLink: {
        textDecoration: 'none',
        transition: 'box-shadow 0.4s',

        '&:hover': {
            opacity: 0.9,
            boxShadow: theme.shadows[2],
            cursor: 'pointer',
        }
    },
    table: {

    },
    tableHead: {
        
    },
    tableRow: {
        
    },
    csvButton: {
        backgroundColor: green[600]
    },
    pdfButton: {
        backgroundColor: red[500]
    },
    filterButton: {
        backgroundColor: blue[500]
    },
    sortIconButton: {
        color: 'inherit',
        
        '&:hover': {
            backgroundColor: 'transparent'
        }
    }
});