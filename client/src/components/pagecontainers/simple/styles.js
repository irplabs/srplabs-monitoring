export default (theme) => ({
    root: {
        display: 'flex',
        flexGrow: 2,
        flexDirection: 'column',
        minWidth: 320, 
    },
    headerRoot: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '20vh',
        minHeight: '120px',
        backgroundColor: theme.palette.primary.main,        
        padding: 4*theme.spacing.unit,
    },
    headerTitle: {
        color: "#FFF",
        textTransform: 'uppercase',
        fontWeight: 'bold',
    },
    contentRoot: {
        padding: 2*theme.spacing.unit,

        '& > *': {
            marginTop: 2*theme.spacing.unit,
            marginBottom: 2*theme.spacing.unit,
        },
    },
});