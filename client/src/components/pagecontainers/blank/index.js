import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, Fade } from '@material-ui/core';
import Loader from '../loader';
import styles from './styles';

const BlankContainer = ({ classes, loading, ...props }) => {
    return (
        loading || false ?
        <div className={classes.root}>
            <Loader />
        </div> :
        <Fade in={true}>
            <div className={classes.root}>
                {props.children}
            </div>
        </Fade>
    )
}

BlankContainer.propTypes = {
    loading: PropTypes.bool,
}

BlankContainer.defaultProps = {
    loading: false,
}

export default withStyles(styles)(BlankContainer);