/**
 * NOTE: This is not complete. Please do not use this.
 * TODO: Move it's development to a separate branch.
 */

import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles, Fade, Typography } from '@material-ui/core';
import Loader from '../loader';
import styles from './styles';

const SimpleTabbedContainer = ({ classes, loading, titleProps, ...props }) => {
    
    return (
        loading || false ?
        <div className={classes.root}>
            <Loader />
        </div> :
        <Fade in={true}>
            <div className={classes.root}>
                
                <div className={classes.headerRoot}>
                    <Typography variant="h4" className={classes.headerTitle}>
                        {titleProps}
                    </Typography>
                </div>
        
                <div className={classes.contentRoot}>
                    {props.children}
                </div>
        
            </div>
        </Fade>
    )
}

SimpleTabbedContainer.propTypes = {
    loading: PropTypes.bool,
    titleProps: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
}


SimpleTabbedContainer.defaultProps = {
    loading: false,
}

export default withStyles(styles)(SimpleTabbedContainer);