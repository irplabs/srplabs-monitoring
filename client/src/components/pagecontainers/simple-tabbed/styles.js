export default (theme) => ({
    root: {
        display: 'flex',
        flexGrow: 2,
        flexDirection: 'column',
    },
    headerRoot: {
        width: '100%',
        height: '20vh',
        minHeight: '120px',
        backgroundColor: theme.palette.primary.main,        
        padding: `${2*theme.spacing.unit}px`,
    },
    headerTitle: {
        color: "#FFF",
    },
    contentRoot: {
        // padding: `${2*theme.spacing.unit}px`,        

        // '& > *': {
        //     marginTop: 2*theme.spacing.unit,
        //     marginBottom: 2*theme.spacing.unit,
        // },
    },
});