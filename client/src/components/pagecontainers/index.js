import Blank from './blank';
import Simple from './simple';
import SimpleTabbed from './simple-tabbed';
import Paper from './paper';

export default { 
    Blank, 
    Simple, 
    SimpleTabbed,
    Paper,
};