import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, Fade, Slide, Paper, Typography } from '@material-ui/core';
import Loader from '../loader';
import styles from './styles';

const PaperContainer = ({ classes, loading, titleProps, ...props }) => {
    return (
        loading || false ?
        <div className={classes.root}>
            <Loader />
        </div> :
        <Fade in={true}>
            <div className={classes.root}>
                
                <div className={classes.headerRoot}>
                    <Typography variant="h4" className={classes.headerTitle}>
                        {titleProps}
                    </Typography>
                </div>

                <Slide in={true} direction="up" >
                    <Paper className={classes.paperRoot}>
                        {props.children}
                    </Paper>
                </Slide>
        
            </div>
        </Fade>
    )
}

PaperContainer.propTypes = {
    loading: PropTypes.bool,
    titleProps: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
}

PaperContainer.defaultProps = {
    loading: false,
}

export default withStyles(styles)(PaperContainer);