import { Provider } from 'react-redux';
import { render } from 'react-dom';
import React from 'react';

import { App } from './apps';
import store from './store';

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
)