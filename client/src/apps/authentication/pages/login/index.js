import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { 
    withStyles, 
    Hidden, 
    Typography, 
    Divider, 
    Paper, 
    Slide, 
    Grow,
    Button,
    TextField
} from '@material-ui/core';

import { login } from '../../actions';
import styles from './styles';
import { redirectUrl } from '../../../../services';

const mapStateToProps = (state, props) => ({
    group: state.authentication.group
});

const mapDispatchToProps = dispatch => ({
    onLogin: (credentials) => dispatch(login(credentials))
});

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            drawer: false,
            credentials: {
                username: '',
                password: ''
            },
        }
    }

    componentDidMount() {
        const { group, history } = this.props;
        if (group!='')
            history.push(redirectUrl[group]);
    }

    toggleDrawer = () => this.setState({ drawer: !this.state.drawer })

    handleForm = name => event => this.setState({
        credentials: {
            ...this.state.credentials,
            [name]: event.target.value
        }
    });

    render() {
        const { credentials } = this.state;
        const { classes, onLogin } = this.props;

        return (
            <div className={classes.container}>
                <div className={classes.infoContainer}>
                    <Grow in={true} timeout={400}>
                        <div className={classes.infoContentContainer}>
                            <img src="../../../../assets/images/fuse.svg" width="200" height="200" />
                            <Hidden smDown>
                                <Typography className={classes.infoText} variant="h4" align="center">Welcome to the IRPLabs</Typography>
                                <Typography className={classes.infoText} align="center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ullamcorper nisl erat, vel convallis elit fermentum pellentesque. Sed mollis velit facilisis facilisis.</Typography>
                            </Hidden>
                        </div>
                    </Grow>
                </div>
                        
                <Slide direction="left" in={true} mountOnEnter unmountOnExit timeout={400}>
                    <Paper className={classes.formContainer}>
                        <Typography variant="h6" className={classes.formTitle}>Login to your account</Typography>
                        <Divider variant="middle" className={classes.formDivider} />
                        <form>
                            <TextField 
                                className={classes.formControl} 
                                variant="outlined" 
                                label="Username" 
                                value={credentials.username} 
                                onChange={this.handleForm('username')} 
                                autoFocus />
                            <TextField 
                                className={classes.formControl} 
                                variant="outlined" 
                                type="password" 
                                label="Password" 
                                value={credentials.password} 
                                onChange={this.handleForm('password')} />
                            <Button 
                                type="submit" 
                                className={classes.formControl} 
                                variant="contained" 
                                color="primary" 
                                onClick={(event) => {
                                    event.preventDefault();
                                    onLogin(this.state.credentials)
                                }} >
                                Login
                            </Button>
                        </form>
                    </Paper>
                </Slide>
            </div>
        )
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login)));