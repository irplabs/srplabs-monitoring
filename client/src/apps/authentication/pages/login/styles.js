export default (theme) => ({
    container: {
        display: 'flex',
        flexDirection: 'row',
        minHeight: '100vh',
        background: 'linear-gradient(to right, #2D323E 0%, rgb(22, 25, 31) 100%)',
        backgroundColor: 'rgb(22, 25, 31)',
        overflow: 'hidden',

        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column',
            overflow: 'initial',
            alignItems: 'center',
        },
    },
    infoContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        flexGrow: 2,
        minHeight: '100vh',
        padding: 24,
        
        [theme.breakpoints.down('sm')]: {
            flexGrow: 'initial',
            minHeight: 240,
        },
        
    },
    infoContentContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '80%',
        
        '& > *': {
            margin: 16,
        },
    },
    infoText: {
        color: '#fff',
    },
    formContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: 360,
        minHeight: '100vh',
        padding: 24,
        backgroundColor: '#fff',
        
        [theme.breakpoints.up('md')]: {
            width: 480,
            borderRadius: 0,
        },

        [theme.breakpoints.down('sm')]: {
            minHeight: '50vh',
        }
    },
    formTitle: {
        textTransform: 'uppercase',
    },
    formDivider: {
        width: '50%',
        height: '2px',
        backgroundColor: '#039be5',
        margin: `${2*theme.spacing.unit}px auto`,
    },
    formControl: {
        width: '100%',
        margin: `${2*theme.spacing.unit}px auto`,
    },
});