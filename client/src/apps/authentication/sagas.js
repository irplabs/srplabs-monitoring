import { take, call, all, put, fork, race, delay } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import C from './actionTypes';    //current app action types
import ui from '../ui';           //import ui action types
import { api, redirectUrl } from '../../services';

function authorize_api_call(credentials) {
    return api.post('token-auth/', credentials).then(response => response.data).catch(error => { throw error });
}

//perform login-logout flow
function* loginFlow() {
    while (true) {
        const { credentials } = yield take(C.LOGIN.INITIATE);

        yield put({ type: ui.actionTypes.TOGGLE_LOADING });

        try {

            const { apiRequest, timeout } = yield race({
                                                        apiRequest: call(authorize_api_call, credentials),
                                                        timeout: delay(5000)       //add 5sec delay for login call
                                                    });

            if (timeout)
                yield put({ 
                    type: ui.actionTypes.ADD_TOAST,
                    payload: { key: Date.now(), message: 'Login Timeout !!', variant: 'warning' }
                });
            else {
                const { status, token, group } = apiRequest;
                const data = { username: credentials.username, group, token };

                if (status == 'LOGIN_SUCCESS') {
                    yield all([
                        put({ type: C.LOGIN.SUCCESS, data }),
                        put({ 
                            type: ui.actionTypes.ADD_TOAST,
                            payload: { key: Date.now(), message: 'Login Successful', variant: 'success' }
                        }),
                        put({ type: ui.actionTypes.TOGGLE_LOADING }),
                        put(push(redirectUrl[group]))
                    ]);

                    yield take(C.LOGOUT.INITIATE);
                    //perform api call to server if necessary and other clearing tasks
                    yield put({ type: C.LOGOUT.SUCCESS });
                }
                else {
                    yield put({
                            type: ui.actionTypes.ADD_TOAST,
                            payload: { key: Date.now(), message: 'Invalid Credentials', variant: 'warning' }
                        });
                }
            }
        }
        catch(error) {
            yield put({
                    type: ui.actionTypes.ADD_TOAST,
                    payload: { key: Date.now(), message: error.message, variant: 'error' }
                });
        }

        yield put({ type: ui.actionTypes.TOGGLE_LOADING });
    }
}

function* root() {
    yield all([
        fork(loginFlow)
    ])
};

export default root;