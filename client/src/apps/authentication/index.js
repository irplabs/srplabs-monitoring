import React, { lazy } from 'react';

import * as constants from './constants';
import reducer from './reducer';
import * as selectors from './selectors';
import saga from './sagas';
import config from './configs';

const Index = lazy(() => import('./pages'));

export default { constants, reducer, selectors, saga, Index, config };