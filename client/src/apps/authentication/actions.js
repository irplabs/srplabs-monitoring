import C from './actionTypes';

const login = (credentials) => ({ type: C.LOGIN.INITIATE, credentials });

export { login };