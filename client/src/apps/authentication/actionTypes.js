import { NAME } from './constants';

export default {
    LOGIN: {
        INITIATE: NAME + "/INITIATE",    //initiate request
        SUCCESS: NAME + "/SUCCESS",      //successful login
    },
    LOGOUT: {
        INITIATE: NAME + "/INITIATE",   //initiate logout
        SUCCESS: NAME + "/SUCCESS"      //successful logout
    }
};