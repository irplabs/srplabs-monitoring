export default {
    navbar: {
        display: false,
    },
    appbar: {
        display: false,
    },
    notifications: {
        display: false,
    },
    footer: {
        display: false,
    },
};