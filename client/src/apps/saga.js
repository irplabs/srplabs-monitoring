import { fork, all } from 'redux-saga/effects';

import authentication from './authentication';
import admin from './admin';

export default function* root() {
    yield all([
        fork(authentication.saga),
        fork(admin.saga)
    ]);
}