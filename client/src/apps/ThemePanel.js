import React from 'react';

export default ({ toggleTheme }) => <button onClick={() => toggleTheme()}>Toggle Theme</button>