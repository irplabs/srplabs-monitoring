import React, { Suspense } from 'react';
import { Route, Redirect } from 'react-router';
import { connect } from 'react-redux';

import ui from '../ui';

const mapStateToProps = (state, props) => ({
    group: state.authentication.group   //add proper login-check, current one is noob's way
});

export default connect(mapStateToProps)(({ component: Component, config, allowedGroup, group, ...rest }) => (
    <Route {...rest} render={(props) => (
        (allowedGroup==undefined || allowedGroup==group)
            ? (
                <ui.Wrapper>
                    <ui.Layout config={config}> {/* Load Layout Changing Panel */}                
                        <Suspense fallback={<div>Loading...</div>}>
                            <Component {...props} />  {/* Loads component requiring logged-in state */}    
                        </Suspense>
                    </ui.Layout>
                </ui.Wrapper>
            )
            : <Redirect to="/login" />
    )} />
));
