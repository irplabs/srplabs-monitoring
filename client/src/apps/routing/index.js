import React from 'react';
import { Switch, Redirect, Route } from 'react-router';

import CustomRoute from './CustomRoute';
import home from '../home';
import authentication from '../authentication';
import admin from '../admin';

export default () => (
    <Switch>
        <CustomRoute exact path="/" component={() => <home.Index /> } config={home.config} />
        <CustomRoute path="/login" component={() => <authentication.Index />} config={authentication.config} />
        <CustomRoute allowedGroup='ADMIN' path="/admin" component={() => <admin.Index />} config={admin.config} />
        <Route render={() => <Redirect to="/login" />} />
    </Switch>
);