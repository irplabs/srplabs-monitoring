import React, { lazy } from 'react';

import config from './configs';

const Index = lazy(() => import('./pages'));

export default { config, Index };