export default {
    navbar: {
        display: true,
    },
    appbar: {
        display: true,
        navbar: true,
        notifications: true,
    },
    notifications: {
        display: true,
    },
    footer: {
        display: true,
    },
};