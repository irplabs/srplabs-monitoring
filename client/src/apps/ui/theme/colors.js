export default {
    main: '#3C4252',
    light: '#7D818C',
    dark: '#1E2129',
    contrastText: "#FFF",
}