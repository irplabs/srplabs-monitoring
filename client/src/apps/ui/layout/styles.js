const styles = theme => ({
    root: {
        position: 'relative',
        display: 'flex',
        flexDirection: 'row',
    },
    leftWrapper: {
    },
    centerWrapper: {
        position: 'relative', // Relative positioning required here, for fixing the toolbar, footer using absolute positioning.
        display: 'flex',
        flexDirection: 'column',
        flexGrow: 2,
        minHeight: '100vh',
        width: '100vw',
    },
    shiftFromTop: {
        marginTop: 64, // Value must be the same as app-bar height.
        minHeight: `calc(100vh - ${64}px)`, // as a consequence of shifting from top.

        [theme.breakpoints.down('xs')]: {
            marginTop: 56, // Value must be the same as app-bar height.
            minHeight: `calc(100vh - ${56}px)`, // as a consequence of shifting from top.
        }
    },
    mainContent: {
        display: 'flex',
        flexDirection: 'inherit', // Basically, the flex-direction is the same as centerWrapper, 'inherit' used to accomodate for future changes.
        flexGrow: 2,
    },
    rightWrapper: {
    },
})

export default styles;