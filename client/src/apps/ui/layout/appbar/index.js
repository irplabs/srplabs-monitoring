import React from 'react';
import { withStyles, AppBar, Toolbar, IconButton } from '@material-ui/core';
import { 
    Menu,
    Notifications
} from '@material-ui/icons';

import styles from './styles';

export default withStyles(styles)(({ classes, config, toggleNavbar, toggleNotifications }) => 
    <AppBar position="fixed" className={classes.root}>
        <Toolbar className={classes.toolbar}>
            <div className={classes.leftPanel}>
                {config.navbar &&
                    <IconButton className={classes.navbarButton} onClick={toggleNavbar()}><Menu /></IconButton>
                }
            </div>
            <div className={classes.rightPanel}>
                {config.notifications &&
                    <IconButton className={classes.notificationButton} onClick={toggleNotifications()}><Notifications /></IconButton>
                }
            </div>
        </Toolbar>
    </AppBar>
);