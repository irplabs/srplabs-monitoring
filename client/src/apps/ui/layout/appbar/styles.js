export default (theme) => ({
    root: {
        backgroundColor: theme.palette.background.appBar,
    },
    toolbar: {
        display: 'flex',
        paddingLeft: 2*theme.spacing.unit,
        paddingRight: 2*theme.spacing.unit,

    },
    leftPanel: {
        flex: 3
    },
    rightPanel: {
        flex: 1
    },
    navbarButton: {

    },
    notificationButton: {
        float: 'right'
    }
});