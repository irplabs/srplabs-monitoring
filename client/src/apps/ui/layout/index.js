import React from 'react';
import classnames from 'classnames';
import { withStyles } from '@material-ui/core';

import Navbar from './navbar';
import AppBar from './appbar';
import Footer from './footer';
import Notifications from './notifications';
import styles from './styles';

class Layout extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            /* Navbar open/close state is held in Layout so that an actionable button may be passed in Toolbar */
            navbar: {
                open: false,
            },
            notifications: {
                open: false,
            },
        };
    }

    /* It's usage requires you to call the function as this.toggleNavbarOpen, while passing it as callback*/
    toggleNavbarOpen = (nextValue = !this.state.navbar.open) => () => 
        this.setState({
            navbar: {
                ...this.state.navbar,
                open: nextValue,
            }
        });

    /* It's usage requires you to call the function as this.toggleNavbaOpen, while passing it as callback*/
    toggleNotificationsOpen = (nextValue = !this.state.notifications.open) => () =>
        this.setState({
            notifications: {
                ...this.state.notifications,
                open: nextValue,
            }
        });

    render() {
        const { classes, config } = this.props;
        const { layout, navbar: navbarItems } = config;

        return (
            <div className={classes.root}>

                <div className={classes.leftWrapper}>
                    {layout.navbar.display && 
                        <Navbar 
                            config={layout.navbar}
                            list={navbarItems}
                            toggle={this.toggleNavbarOpen}
                            open={this.state.navbar.open}
                        />
                    }
                </div>

                <div className={classnames(
                                            classes.centerWrapper,
                                            layout.appbar.display && classes.shiftFromTop
                                        )}>
                    {layout.appbar.display && 
                        <AppBar 
                            config={layout.appbar}
                            toggleNavbar={this.toggleNavbarOpen}
                            toggleNotifications={this.toggleNotificationsOpen}
                        />
                    }
                    
                    <main className={classes.mainContent}>
                        {this.props.children}
                    </main>
                    
                    {layout.footer.display && 
                        <Footer 
                            config={layout.footer}
                        />
                    }
                </div>

                <div className={classes.rightWrapper}>
                    {layout.notifications.display && 
                        <Notifications 
                            config={layout.notifications}
                            toggle={this.toggleNotificationsOpen}
                            open={this.state.notifications.open}
                        />
                    }
                </div>

            </div>
        )
    }
}

export default withStyles(styles)(Layout);