import React from 'react';
import { withStyles, SwipeableDrawer } from '@material-ui/core';

import styles from './styles';

export default withStyles(styles)(({ classes, open, toggle }) => 
    <div className={classes.root}>
        <SwipeableDrawer
            anchor="right"
            open={open}
            onOpen={toggle(true)}
            onClose={toggle(false)}
        >
            <div style={{
                width: '250px',
            }}>
            </div>
        </SwipeableDrawer>
    </div>
);