import React from 'react';
import { withStyles, SwipeableDrawer, IconButton, Typography, Toolbar } from '@material-ui/core';
import { Close } from '@material-ui/icons';

import User from './user';
import NavList from './nav-list';
import styles from './styles';

export default withStyles(styles)(({ classes, open, list, toggle, navigation, ...props }) => 
    <SwipeableDrawer open={open} onOpen={toggle(true)} onClose={toggle(false)}>
        {/* The UI seen */}
        <div className={classes.drawerRoot}>
            
        
            {/* Some title, logo to be shown. */}
            <Toolbar className={classes.siteInfoWrapper}>
                
                {/* Close icon for navbar */}
                <IconButton className={classes.drawerCloseButton} onClick={toggle()}><Close /></IconButton>
            
            </Toolbar>

            {/* User Information */}
            <div className={classes.userInfoWrapper}>
                <User />
            </div>

            {/* Navigation Links */}
            <div className={classes.navListWrapper}>
                <NavList list={list} level={1} toggle={toggle} />
            </div>
        </div>

    </SwipeableDrawer>
);