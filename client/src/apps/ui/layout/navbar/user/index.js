import React from 'react';
import classnames from 'classnames';
import { withStyles, Typography } from '@material-ui/core';
import styles from './styles';

/* TODO: I guess, connect with redux store to get user data - image-path/object, username/Name, email, department, interests, et cetera */

export default withStyles(styles)(({ classes, ...props }) => {
    return (
        <div className={classes.root}>
            <img className={classnames(classes.image, classes.roundedImage)} />

            <Typography color="inherit">Name</Typography>
            <Typography color="inherit">Some more</Typography>
        </div>
    )
})
