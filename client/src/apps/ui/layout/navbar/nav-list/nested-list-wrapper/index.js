import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, Collapse } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import NavList from '../index';
import NavListItem from '../nav-list-item';
import styles from './styles';

class NestedListWrapper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
    }

    toggleCollapse = () => {
        this.setState({
            open: !this.state.open,
        });
    }

    render() {
        const { item, toggle, level, classes, ...restProps } = this.props;
        const { open } = this.state;

        return (
            <React.Fragment>
                <NavListItem level={level} variant="item" title={item.title} icon={item.icon} onClick={this.toggleCollapse}>
                    {open ? <ExpandLess /> : <ExpandMore />}
                </NavListItem>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <NavList level={level + 1} list={item.list} toggle={toggle} />
                </Collapse>
            </React.Fragment>
        )
    }
}

NestedListWrapper.propTypes = {
    item: PropTypes.object.isRequired,
    toggle: PropTypes.func.isRequired,
    level: PropTypes.number.isRequired,
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(NestedListWrapper);