export default (theme) => ({
    root: {
        /* Override default anchor tag.i.e.<a /> styles */
        textDecoration: 'none',
        color: 'white',
        width: 'calc(100% - 16px)',
        height: 48,
        /* order:  top right bottom left*/
        borderRadius: '0 25px 25px 0',

        '&.active': {
            backgroundColor: '#039be5',
            color: 'white',
        },

    },
    primaryContent: {
        color: 'inherit',
    }
})