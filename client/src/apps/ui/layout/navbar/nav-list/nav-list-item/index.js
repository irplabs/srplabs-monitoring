import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { withStyles, withTheme, ListItem, ListItemText } from '@material-ui/core';
import styles from './styles';

const NavListItem = ({ variant, title, url, icon, onClick, level, classes, theme, ...props }) => {
    let componentProps = {
        style: (level) ? {
            paddingLeft: level * 2 * theme.spacing.unit,
        } : {},
    };

    if(variant === "link") {
        componentProps = {
            ...componentProps,
            component: NavLink,
            to: url,
            activeClassName: "active",
            exact: true,
        }
    }
    else { // variant === "item"
        componentProps = {
            ...componentProps,
            component: "li",
        }
    }

    return (
        <ListItem 
            button
            {...componentProps}
            className={classes.root}
            onClick={onClick}
            {...props}
            >
            {icon}
            <ListItemText inset primary={title} classes={{ 
                primary: classes.primaryContent,
            }}/>
            {props.children}
        </ListItem>
    )
};

NavListItem.propTypes = {
    variant: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    url: PropTypes.string,
    icon: PropTypes.node,
    onClick: PropTypes.func.isRequired,
    level: PropTypes.number.isRequired,
    classes: PropTypes.object.isRequired,
}

NavListItem.defaultProps = {
    variant: "link",
}

export default withTheme()(withStyles(styles)(NavListItem));