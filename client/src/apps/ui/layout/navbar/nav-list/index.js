import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, List } from '@material-ui/core';
import NestedListWrapper from './nested-list-wrapper';
import NavListItem from './nav-list-item';
import styles from './styles';

const NavList = ({ list, level, toggle, classes, ...props}) => {
    return (
        <List className={classes.root}>
            {list.map((item, index) => 
                item.nesting ? 
                    <NestedListWrapper 
                        key={index} 
                        item={item} 
                        toggle={toggle}
                        level={level} /> :
                    <NavListItem 
                        key={index} 
                        variant="link" 
                        title={item.title} 
                        url={item.url} 
                        icon={item.icon} 
                        onClick={toggle()}
                        level={level} />)}
        </List>
    )
};

NavList.propTypes = {
    nested: PropTypes.bool,
    list: PropTypes.array.isRequired,
    toggle: PropTypes.func.isRequired,
    /**
     * 'level' is rippled down all the way to NavListItem, where
     * it is used to add left padding on list-items.
     * 'level' is incremented at NestedListWrapper.
     */
    level: PropTypes.number.isRequired,
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(NavList);