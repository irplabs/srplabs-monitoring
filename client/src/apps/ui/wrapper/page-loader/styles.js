export default (theme) => ({
    linearIndeterminateRoot: {
        position: 'fixed',
        top: 0,
        left: 0,
        flexGrow: 1,
        width: '100%',
        zIndex: 2100,

        [theme.breakpoints.down('md')] : {
            height: '3px', // default value is 4px
        }
    },
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 2000,
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        width: '100vw',
        height: '100vh',
    },
})