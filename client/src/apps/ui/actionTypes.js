export default {
    CHANGE_THEME: "CHANGE_THEME",   //toggle theme,
    ADD_TOAST: "ADD_TOAST",    //add toast notifications,
    TOGGLE_LOADING: "TOGGLE_LOADING" 
};