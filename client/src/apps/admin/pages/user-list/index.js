import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchUsers } from '../../actions';
import { Table } from '../../../../components';

const mapStateToProps = (state, props) => ({
    users: state.admin.users
});

const mapDispatchToProps = dispatch => ({
    onFetchUsers: () => dispatch(fetchUsers()),
});

class Users extends Component {
    constructor(props) {
        super(props);

        this.state = {
            table: {
                columns: [
                    { name: 'Username', type: 'string', filter: true, sort: true },
                    { name: 'Name', type: 'string', filter: true, sort: true },
                    { name: 'Phone', type: 'string', filter: true, sort: true },
                    { name: 'Email', type: 'string', filter: true, sort: true },
                ],
                rows: [],
            },
        };
    }

    setUp = (users) => {
        let rows = [];
        users.map(each => {
            rows.push({ data: [each.user.username, each.name, each.phone, each.email] });
        });

        this.setState({
            table: {
                ...this.state.table,
                rows,
            }
        });
    }

    shouldComponentUpdate({ users: newPropUsers }, { table: newTable }) {
        const { users: currentPropUsers } = this.props;
        const { table: currentTable } = this.state;

        if (newPropUsers != currentPropUsers) {
            this.setUp(newPropUsers);
            return true;
        }
        else if (currentTable.rows != newTable.rows)
            return true;
        else
            return false;
    }
    
    componentDidMount() {
        const { onFetchUsers, users } = this.props;
        
        if (!users.length) 
            onFetchUsers();
        else
            this.setUp(users);
    }

    render() {
        const { table } = this.state;

        return (
            <Table indexing rows={table.rows} columns={table.columns} />
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);