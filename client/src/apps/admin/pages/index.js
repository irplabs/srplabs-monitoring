import React from 'react';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';

import AddUser from './add-user';
import UserList from './user-list';
import User from './user';
import SocietyList from './society-list';

import { NAME } from '../constants';
import reducer from '../reducer';
import { reducerRegistry } from '../../../services';

reducerRegistry.register(NAME, reducer);

export default withRouter(({ match, ...restProps }) => 
    /* Switch is essential to choose only one route. Useful for only rendering Index content when further url doesn't exist.*/
    <Switch>   
        <Route exact path={`${match.url}/users/add`} component={AddUser} />
        <Route exact path={`${match.url}/users/:userId`} component={User} />
        <Route exact path={`${match.url}/users`} component={UserList} />
        <Route exact path={`${match.url}/societies`} component={SocietyList} />
    </Switch>
);