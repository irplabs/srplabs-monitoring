export default theme => ({
    card: {
        position: 'relative',
        width: '100%',
        // title + image + bottom
        height: 'calc(64px + 180px + 48px + 5px)',
        
        [theme.breakpoints.down('md')]: {
            height: 'calc(64px + 180px + 48px + 5px)',
        },
    },
    imageContainer: {
        position: 'relative',
        height: 180,

        [theme.breakpoints.down('md')]: {
            height: 180,
        },
    },
    image: {
        width: '100%',
        height: '100%',
    },
    cardTitleContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        padding: theme.spacing.unit,
        backgroundColor: 'rgb(33, 150, 243)',
        color: theme.palette.getContrastText('rgb(33, 150, 243)'),
    },  
    title: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: '1.5em',
    },
    secondaryTextContainer: {
        display: 'flex',
        alignItems: 'center',
        height: 48,

        '& > p': {
            marginRight: theme.spacing.unit,
        },
    },
})