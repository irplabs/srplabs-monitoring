import React from 'react';
import classnames from 'classnames';

import {
    withStyles,
    withTheme,
    Card,
    CardHeader,
    CardMedia,
    CardContent,
    CardActions,
    CardActionArea,
    Collapse,
    Avatar,
    IconButton,
    Typography,
    Divider,
    Button,
    LinearProgress,
    Grid,
    Icon,
} from '@material-ui/core';

import {
    AccessTime,
    Favorite as FavoriteIcon,
    Share as ShareIcon,
    ExpandMore as ExpandMoreIcon,
    MoreVert as MoreVertIcon,
} from '@material-ui/icons';

import Palette from 'react-palette';

import { Containers } from '../../../../components';
import styles from './styles';

class SocietyList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: false 
        };
    }

    handleExpandClick = () => {
        this.setState({
            expanded: !this.state.expanded,
        })
    }

    render() {
        const { classes, theme } = this.props;

        return (
            <Containers.Simple titleProps="Societies">
                <Grid container spacing={16} item xs={12} style={{ margin: '0 auto' }}>
                    
                    <Grid item xs={12} sm={6} md={4} lg={3}>
                        <Card elevation={1} className={classes.card}>
                            <CardActionArea>

                            
                            <Palette image="https://hindi.oneindia.com/img/2017/10/4-06-1507261195.jpg">
                                    {palette => (
                                        <React.Fragment>
                                            <div 
                                                className={classes.cardTitleContainer} 
                                                style={{ backgroundColor: palette.vibrant }}
                                            >
                                                <Typography 
                                                    style={{ color: theme.palette.getContrastText(palette.vibrant) }}
                                                    className={classes.title}
                                                    >
                                                    Kendriya Vidyalaya
                                                </Typography>
                                            </div>
                                            <div className={classes.imageContainer}>
                                                <img className={classes.image} src="https://hindi.oneindia.com/img/2017/10/4-06-1507261195.jpg" />
                                            </div>
                                            <Divider/> 
                                            <div className={classes.secondaryTextContainer}>
                                                <Typography className={classes.text}>47</Typography>
                                                <Typography color="textSecondary">Registered Schools</Typography>
                                            </div>
                                            <LinearProgress
                                                variant="determinate"
                                                value={47 * 100 / 100}
                                                color="secondary"
                                            /> 
                                        </React.Fragment>
                                    )}
                                </Palette>


                                {/* <CardMedia
                                    className={classes.media}
                                    image="https://hindi.oneindia.com/img/2017/10/4-06-1507261195.jpg"
                                    title="Kendriya Vidayala"
                                />
                                <Divider/> */}
                                {/* <CardContent className={classes.secondaryTextContainer}>
                                    <Typography className={classes.text}>47</Typography>
                                    <Typography color="textSecondary">Registered Schools</Typography>
                                </CardContent>
                                */}
                            </CardActionArea>
                        </Card>
                    </Grid>

                </Grid>
                
            </Containers.Simple>
        )
    }
} 

export default withTheme()(withStyles(styles)(SocietyList));