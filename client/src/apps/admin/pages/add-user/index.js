import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
    MenuItem, 
    withStyles, 
    Divider, 
    TextField, 
    Button,
    Typography,
} from '@material-ui/core';
import styles from './styles';

import { Containers } from '../../../../components';
import { addUser, fetchGroups } from '../../actions';

const mapStateToProps = (state, props) => ({
    groups: state.admin.groups,
});

const mapDispatchToProps = dispatch => ({
    onAddUser: (data) => dispatch(addUser(data)),
    onfetchGroups: () => dispatch(fetchGroups())
})

class AddUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            form: {
                username: {
                    label: "Username",
                    value: "",
                    errorMessage: "",
                },
                email: {
                    label: "Email",
                    value: "",
                    errorMessage: ""
                },
                name: {
                    label: "Name",
                    value: "",
                    errorMessage: "",
                },
                phone: {
                    label: "Phone",
                    value: "",
                    errorMessage: "",
                },
                group: {
                    label: "Group",
                    value: "",
                    errorMessage: "",
                },
            },
        };
    }

    componentDidMount() {
        const { groups, onfetchGroups } = this.props;

        if (!groups.length) {
            onfetchGroups();
        }
    }

    phoneValidator = () => {
        const { value } = this.state.form.phone;
        if (value.length!=0 && value.length!=10) {
            this.setState({
                form: {
                    ...this.state.form,
                    phone: {
                        ...this.state.form.phone,
                        errorMessage: "Invalid Phone Number"
                    }
                }
            })
        }
    }

    emailValidator = () => {
        // RegEx picked from here - https://stackoverflow.com/a/46181/3971277
        var emailRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const { value } = this.state.form.email;
        if(value.length!=0 && !emailRegEx.test(value)) {
            this.setState({
                form: {
                    ...this.state.form,
                    email: {
                        ...this.state.form.email,
                        errorMessage: "Invalid Email Address"
                    }
                }
            })
        }
    }

    handleChange = (attributeName) => (event) => {
        this.setState({
            form: {
                ...this.state.form,
                [attributeName]: {
                    ...this.state.form[attributeName],
                    value: event.target.value,
                    errorMessage: ""
                }
            }
        });
    }

    checkValid = () => {
        let { form } = this.state;
        let isValid = true;

        console.log(form);

        for (let key in form) {
            if (!form[key].value.length) {
                form[key].errorMessage = form[key].label + ' is required';
                isValid = false;
            }

            isValid = isValid && !form[key].errorMessage.length;
        }

        this.setState({ form });

        return isValid;
    }

    submit = (event) => {
        event.preventDefault();
        const { form } = this.state;
        const { onAddUser } = this.props;

        if (this.checkValid()) {
            let data = {};
            
            for (let key in form) {
                if (key == "name")
                    data[key] = form[key].value.toUpperCase();
                else
                    data[key] = form[key].value;
            }

            onAddUser(data);
        }
    }

    render() {
        const {
            classes,
            groups,
        } = this.props;

        const { 
            form,
        } = this.state;

        return (
            <Containers.Simple loading={this.state.loading} titleProps="User Registration">

                <div className={classes.multipleUsers}>
                    <Typography className={classes.title}>Multiple Users</Typography>
                    <Button variant="contained" color="primary">Upload File</Button>
                </div>

                <div className={classes.dividerContainer}>
                    <Divider className={classes.divider} />
                    <Typography>OR</Typography>
                    <Divider className={classes.divider} />
                </div>
                
                <form className={classes.form}>
                    <Typography className={classes.title}>Single User</Typography>
                    
                    <TextField
                        className=""
                        label={form.username.label}
                        id="username"
                        name="username"
                        type="text"
                        value={form.username.value}
                        onChange={this.handleChange('username')}
                        error={Boolean(form.username.errorMessage)}
                        helperText={form.username.errorMessage}
                        variant="outlined"
                        autoFocus
                        fullWidth
                    />

                    <TextField
                        className=""
                        label={form.name.label}
                        id="name"
                        name="name"
                        type="text"
                        value={form.name.value}
                        onChange={this.handleChange('name')}
                        error={Boolean(form.name.errorMessage)}
                        helperText={form.name.errorMessage}
                        variant="outlined"
                        fullWidth
                    />

                    <TextField
                        className=""
                        label={form.phone.label}
                        id="phone"
                        name="phone"
                        type="number"
                        value={form.phone.value}
                        onChange={this.handleChange('phone')}
                        onBlur={this.phoneValidator}
                        error={Boolean(form.phone.errorMessage)}
                        helperText={form.phone.errorMessage}
                        variant="outlined"
                        fullWidth
                    />

                    <TextField
                        className=""
                        label={form.email.label}
                        id="email"
                        name="email"
                        type="text"
                        value={form.email.value}
                        onChange={this.handleChange('email')}
                        onBlur={this.emailValidator}
                        error={Boolean(form.email.errorMessage)}
                        helperText={form.email.errorMessage}
                        variant="outlined"
                        fullWidth
                    />

                    <TextField
                        className=""
                        label={form.group.label}
                        id="group"
                        name="group"
                        type="text"
                        value={form.group.value}
                        onChange={this.handleChange('group')}
                        error={Boolean(form.group.errorMessage)}
                        helperText={form.group.errorMessage}
                        variant="outlined"
                        fullWidth
                        select
                    >
                        {groups.map(each => <MenuItem key={each.id} value={each.name}>{each.name}</MenuItem>)}
                    </TextField>
                    
                    <Button variant="contained" color="primary" type="submit" onClick={(event) => this.submit(event)}>Submit</Button>
                </form>

            </Containers.Simple>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AddUser));