export default (theme) => ({
    multipleUsers: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 0,
        width: '80%',
        marginLeft: 'auto',
        marginRight: 'auto',

        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
    },
    title: {
        width: '100%',
        fontSize: '1em',
        fontWeight: 'bold',
    },
    dividerContainer: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: '80%',
        marginLeft: 'auto',
        marginRight: 'auto',

        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
    },
    divider: {
        width: '40%',
        height: 1,
        backgroundColor: theme.palette.primary.main,
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '80%',
        marginLeft: 'auto',
        marginRight: 'auto',

        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },

        '& > *': {
            marginTop: theme.spacing.unit,
            marginBottom: theme.spacing.unit,
        }
    },
});