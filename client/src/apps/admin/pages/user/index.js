import React from 'react';
import { withRouter } from 'react-router-dom';

class User extends React.Component {
    constructor(props) {
        super(props);
        
        // Extracting the userId from url.
        this.user = this.props.match.params.userId;
        
        this.state = {};
    }

    render() {
        return (
            <div>
                One single user.
            </div>
        );
    }
}

export default withRouter(User);