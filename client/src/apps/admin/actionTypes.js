import { NAME } from './constants'; 

export default {
    USERS: {
        ADD: {
            REQUEST: NAME+"/USERS/ADD/REQUEST",
        },
        FETCH: {
            REQUEST: NAME+"/USERS/FETCH/REQUEST",
            RESPONSE: NAME+"/USERS/FETCH/RESPONSE",
        },
    },
    GROUPS: {
        REQUEST: NAME+"/GROUPS/REQUEST",
        RESPONSE: NAME+"/GROUPS/RESPONSE",
    }
};