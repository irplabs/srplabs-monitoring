import navbar from './navbar';
import layout from './layout';

export default { navbar, layout };