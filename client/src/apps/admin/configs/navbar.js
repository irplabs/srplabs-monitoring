import React from 'react';
import { Home, Dashboard, People, PeopleOutline, PersonAdd } from '@material-ui/icons';

export default [
    {
        url: '/',
        title: 'Home',
        icon: <Home />,
    },
    {
        url: '/admin/analytics',
        title: 'Analytics',
        icon: <Dashboard />,
    },
    {
        nesting: true,
        title: 'Users',
        icon: <PeopleOutline />,
        list: [
            {
                url: '/admin/users',
                title: 'All Users',
                icon: <People />,
            },
            {
                url: '/admin/users/add',
                title: 'Create User',
                icon: <PersonAdd />
            },
        ]
    },
]