import C from './actionTypes';

export const addUser = (data) => ({ type: C.USERS.ADD.REQUEST, data });

export const fetchGroups = () => ({ type: C.GROUPS.REQUEST });

export const fetchUsers = () => ({ type: C.USERS.FETCH.REQUEST });
