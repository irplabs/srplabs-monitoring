import { take, call, all, fork, put, race, delay } from 'redux-saga/effects';
import { push } from 'connected-react-router';

import C from './actionTypes';    //current app action types
import ui from '../ui';           //import ui action types
import { api } from '../../services';

function addUser_api_call(data) {
    return api.post('person/add/', data).then(response => response.data).catch(error => { throw error });
}

function getGroups_api_call() {
    return api.get('groups/').then(response => response.data).catch(error => { throw error });
}

function getUsers_api_call() {
    return api.get('persons/').then(response => response.data).catch(error => { throw error });
}

function* getUsers() {
    while (true) {
        yield take(C.USERS.FETCH.REQUEST);

        yield put({ type: ui.actionTypes.TOGGLE_LOADING });

        try {
            const { apiRequest, timeout } = yield race({
                                                        apiRequest: call(getUsers_api_call),
                                                        timeout: delay(5000)
                                                    });

            if (timeout)
                yield put({
                    type: ui.actionTypes.ADD_TOAST,
                    payload: { key: Date.now(), message: 'Operation Timeout !!', variant: 'warning' }
                });
            else {
                const { status, persons } = apiRequest;

                if (status == 'SUCCESS')
                    yield put({
                        type: C.USERS.FETCH.RESPONSE,
                        payload: persons
                    });
                else {
                    yield put({
                        type: ui.actionTypes.ADD_TOAST,
                        payload: { key: Date.now(), message: status, variant: 'warning' }
                    });
                }
            }
        }
        catch (error) {
            yield put({
                type: ui.actionTypes.ADD_TOAST,
                payload: { key: Date.now(), message: error.message, variant: 'error' }
            });
        }

        yield put({ type: ui.actionTypes.TOGGLE_LOADING });
    }
}

function* getGroups() {
    while (true) {
        yield take(C.GROUPS.REQUEST);

        yield put({ type: ui.actionTypes.TOGGLE_LOADING });

        try {

            const { apiRequest, timeout } = yield race({
                                                        apiRequest: call(getGroups_api_call),
                                                        timeout: delay(5000)       //add 5sec delay for login call
                                                    });

            if (timeout)
                yield put({ 
                    type: ui.actionTypes.ADD_TOAST,
                    payload: { key: Date.now(), message: 'Operation Timeout !!', variant: 'warning' }
                });
            else {
                const { status, groups } = apiRequest;

                if (status == 'SUCCESS')
                    yield put({
                            type: C.GROUPS.RESPONSE,
                            payload: groups
                        });
                else {
                    yield put({
                        type: ui.actionTypes.ADD_TOAST,
                        payload: { key: Date.now(), message: status, variant: 'warning' }
                    });
                }
            }
        }
        catch(error) {
            yield put({
                    type: ui.actionTypes.ADD_TOAST,
                    payload: { key: Date.now(), message: error.message, variant: 'error' }
                });
        }

        yield put({ type: ui.actionTypes.TOGGLE_LOADING });
    }
}

//perform add-user flow
function* addUser() {
    while (true) {
        const { data } = yield take(C.USERS.ADD.REQUEST);

        yield put({ type: ui.actionTypes.TOGGLE_LOADING });

        try {

            const { apiRequest, timeout } = yield race({
                                                        apiRequest: call(addUser_api_call, data),
                                                        timeout: delay(5000)       //add 5sec delay for login call
                                                    });

            if (timeout)
                yield put({ 
                    type: ui.actionTypes.ADD_TOAST,
                    payload: { key: Date.now(), message: 'Operation Timeout !!', variant: 'warning' }
                });
            else {
                const { status } = apiRequest;

                if (status == 'SUCCESS') {
                    yield all([
                        put({ 
                            type: ui.actionTypes.ADD_TOAST,
                            payload: { key: Date.now(), message: 'User Added Successfully', variant: 'success' }
                        }),
                        put(push('/admin/users/add')),
                    ]);
                }
                else {
                    yield put({
                            type: ui.actionTypes.ADD_TOAST,
                            payload: { key: Date.now(), message: status, variant: 'warning' }
                        });
                }
            }
        }
        catch(error) {
            yield put({
                    type: ui.actionTypes.ADD_TOAST,
                    payload: { key: Date.now(), message: error.message, variant: 'error' }
                });
        }

        yield put({ type: ui.actionTypes.TOGGLE_LOADING });
    }
}

function* root() {
    yield fork(addUser);
    yield fork(getGroups);
    yield fork(getUsers);
}

export default root;