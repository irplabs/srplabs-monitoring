import C from './actionTypes';
import { combineReducers } from 'redux';

const groups = (state=[], action) => {
    switch(action.type) {
        case C.GROUPS.RESPONSE:
            return action.payload;
        default:
            return state;
    }
}

const users = (state = [], action) => {
    switch(action.type) {
        case C.USERS.FETCH.RESPONSE:
            return action.payload;
        default:
            return state;
    }
}

export default combineReducers({
    groups,
    users,
});