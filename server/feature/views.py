# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView,CreateAPIView
from feature.serializers import*
from utility.authenticate import* 
from feature.models import*


class FeatureListView(APIView):

    def get(self,request):
        access_group = ['ADMIN']
        if not isAuthenticated(request,access_group):
            return Response({'STATUS':'FORBIDDEN'})

        filters ={}
        features = Feature.objects.filter(**filters)
        serializer = FeatureSerializer(features,many=True)
        return Response({'status': 'SUCCESS', 'features': serializer.data})

    


	


