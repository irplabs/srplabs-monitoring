# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

class Feature(models.Model):

	# DATABASE FIELDS
    feature_id =  models.CharField(max_length=20,primary_key=True)
    name = models.CharField(max_length=200)
    desc= models.TextField(max_length=500)

   
	# META CLASS
    class Meta:
        verbose_name = 'Feature'
        verbose_name_plural = 'Features'

	# TO STRING METHOD
    def __str__(self):
        return str(self.feature_id) + "-" + str(self.name)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        #do_something
        super().save(*args, **kwargs)  # Call the "real" save() method.
        #do_something