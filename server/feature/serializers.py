from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from feature.models import*
from rest_framework import*

class FeatureSerializer(ModelSerializer):

	class Meta:
		model = Feature
		fields = "__all__"
