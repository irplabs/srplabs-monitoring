# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import*
from import_export import resources


# Register your models here.

@admin.register(Organization)
class OrganizationAdmin(ImportExportModelAdmin):
    pass

@admin.register(Person)
class PersonAdmin(ImportExportModelAdmin):
    pass

@admin.register(Address)
class AddressAdmin(ImportExportModelAdmin):
    pass

@admin.register(Board)
class BoardAdmin(ImportExportModelAdmin):
    pass