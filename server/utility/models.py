# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from datetime import date
from decimal import Decimal


class Address(models.Model):

	# DATABASE FIELDS
	street_address = models.TextField(max_length=500)
	pincode = models.CharField(max_length=20)
	city = models.CharField(max_length=50)
	state = models.CharField(max_length=50)
	country = models.CharField(max_length=50)
	

	# META CLASS
	class Meta:
		verbose_name = 'Address'
		verbose_name_plural = 'Addresses'

	# TO STRING METHOD
	def __str__(self):
		return str(self.city) + "-" + str(self.state)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

# Create your models here.

class Organization(models.Model):

	# DATABASE FIELDS
	user = models.OneToOneField(User, on_delete=models.CASCADE,blank=True,null=True)
	name = models.CharField(max_length=200)
	contact = models.CharField(max_length=20)
	email = models.EmailField(max_length=70,blank=True,unique=True)
	address = models.ForeignKey(Address,on_delete=models.CASCADE,null=True)

   
	# META CLASS
	class Meta:
		verbose_name = 'Organization'
		verbose_name_plural = 'Organizations'

	# TO STRING METHOD
	def __str__(self):
		return str(self.name) + "-" + str(self.contact)

    # SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class Person(models.Model):

	# DATABASE FIELDS
	user = models.OneToOneField(User, on_delete=models.CASCADE,blank=True,null=True)																																																																																																																																																																																																																																																																																																																																																																																																																							
	name = models.CharField(max_length=200)
	email = models.EmailField(max_length=70,blank=True,unique=True)
	phone = models.CharField(max_length=20)

	# META CLASS
	class Meta:
		verbose_name = 'Person'
		verbose_name_plural = 'Persons'

	# TO STRING METHOD
	def __str__(self):
		return str(self.name) + "-" + str(self.email)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something

class Board(models.Model):

	# DATABASE FIELDS
	board_id = models.CharField(max_length=20,primary_key=True)
	name = models.CharField(max_length=200)

	# META CLASS
	class Meta:
		verbose_name = 'Board'
		verbose_name_plural = 'Boards'

	# TO STRING METHOD
	def __str__(self):
		return str(self.board_id) + "-" + str(self.name)

	# SAVE METHOD
	def save(self, *args, **kwargs):
		#do_something
		super().save(*args, **kwargs)  # Call the "real" save() method.
		#do_something