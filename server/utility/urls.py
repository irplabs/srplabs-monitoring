from django.conf.urls import url
from django.urls import path, include
from django.contrib import admin
from utility.views import CustomAuthToken
from utility.views import*


urlpatterns = [

    url(r'^api/token-auth/', CustomAuthToken.as_view()),
    url(r'^api/groups/',GroupListView.as_view(),name='view-all'),
    url(r'api/person/add/$', addperson, name='create'),
    url(r'^api/users/',UserListView.as_view(),name='view-all'),
    url(r'^api/persons/',PersonListView.as_view(),name='view-all'),
    

]