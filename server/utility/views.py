# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView,CreateAPIView
from utility.models import*
from utility.serializers import*
from utility.authenticate import*
from server.settings import SECRET_KEY
import jwt

class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
    	try:
    		serializer = self.serializer_class(data=request.data,context={'request': request })
    		serializer.is_valid(raise_exception=True)

    		user = serializer.validated_data['user']
    		ip = get_client_ip(request)
    		jwt_token = jwt.encode({ 'username': user.username,'ip': ip },SECRET_KEY,algorithm = 'HS256').decode('utf-8')

    		group = Group.objects.get(user=user)
    		return JsonResponse({ 'status': 'LOGIN_SUCCESS', 'token': jwt_token, 'group': group.name })

    	except Exception as e:
    		print(str(e))
    		return Response({ 'status': 'LOGIN_FAILED' })

class GroupListView(ListAPIView):
	queryset = Group.objects.all()
	serializer_class = GroupSerializer

	def list(self,request):
		access_group = ['ADMIN']
		if not isAuthenticated(request,access_group):
			return Response({'STATUS':'FORBIDDEN'})


		queryset = self.get_queryset()
		serializer = GroupSerializer(queryset,many=True)
		return Response({ 'status': 'SUCCESS', 'groups': serializer.data })

class UserListView(APIView):

	def get(self,request):

		access_group = ['ADMIN']
		if not isAuthenticated(request,access_group):
			return Response({'STATUS':'FORBIDDEN'})

		filters = {}

		users = User.objects.filter(**filters)
		serializer = UserSerializer(users,many=True)
		return Response({'status': 'SUCCESS', 'users': serializer.data})

class PersonListView(APIView):

	def get(self,request):

		access_group = ['ADMIN']
		if not isAuthenticated(request,access_group):
			return Response({'STATUS':'FORBIDDEN'})

		filters = {}

		persons = Person.objects.filter(**filters)
		serializer = PersonSerializer(persons,many=True)
		return Response({'status': 'SUCCESS', 'persons': serializer.data})

@csrf_exempt
def addperson(request):
	if request.method == 'POST':
		print(request.body.decode())
		return JsonResponse({'status':"SUCCESS"})
	


