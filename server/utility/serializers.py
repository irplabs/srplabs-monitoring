from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from utility.models import*
from rest_framework import*
from django.contrib.auth.models import Group,User

class GroupSerializer(ModelSerializer):

	class Meta:
		model = Group
		fields = "__all__"

class AddressSerializer(ModelSerializer):

	class Meta:
		model = Address
		fields = "__all__"		

class UserSerializer(ModelSerializer):

	class Meta:
		model = User
		fields = ('username','is_active','groups')																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																								

class OrganizationSerializer(ModelSerializer):

	user = UserSerializer(read_only=True)
	address = AddressSerializer(read_only=True)
	class Meta:
		model = Organization
		fields = "__all__"



class PersonSerializer(ModelSerializer):
	
	user = UserSerializer(read_only=True)
	class Meta:
		model = Person
		exclude = ('id',)

	

