# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import date
from decimal import Decimal
from feature.models import * 
from school.models import *
from utility.models import * 


class Subscription(models.Model):

	# DATABASE FIELDS
    school = models.ForeignKey(School, on_delete=models.CASCADE,blank=True,null=True)
    feature = models.ForeignKey(Feature, on_delete=models.CASCADE,blank=True,null=True)
    verified_by = models.ForeignKey(Person, on_delete=models.CASCADE,blank=True,null=True)
    date = models.DateField(default=date.today)
    charge = models.DecimalField(max_digits=8,decimal_places=2,blank=True)

   
	# META CLASS
    class Meta:
        verbose_name = 'Subscription'
        verbose_name_plural = 'Subscriptions'

    # TO STRING METHOD
    def __str__(self):
        return str(self.school) + "-" + str(self.charge)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        #do_something
        super().save(*args, **kwargs)  # Call the "real" save() method.
        #do_something