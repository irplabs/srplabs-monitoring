# Generated by Django 2.0.3 on 2019-03-02 12:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('utility', '0001_initial'),
        ('region', '0003_auto_20190302_1210'),
    ]

    operations = [
        migrations.CreateModel(
            name='School',
            fields=[
                ('code', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('board', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='utility.Board')),
                ('organization', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='utility.Organization')),
                ('region', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='region.Region')),
            ],
            options={
                'verbose_name_plural': 'Schools',
                'verbose_name': 'School',
            },
        ),
    ]
