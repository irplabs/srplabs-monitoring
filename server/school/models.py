# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from region.models import *
from utility.models import *
from datetime import date
from decimal import Decimal

class School(models.Model):

    # DATABASE FIELDS
    code = models.CharField(max_length=20,primary_key=True)
    organization = models.ForeignKey(Organization,on_delete=models.CASCADE,null=True)
    region = models.ForeignKey(Region,on_delete=models.CASCADE,null=True)
    board = models.ForeignKey(Board,on_delete=models.CASCADE,null=True)
    
    # META CLASS
    class Meta:
        verbose_name = 'School'
        verbose_name_plural = 'Schools'

    # TO STRING METHOD
    def __str__(self):
        return str(self.code) + "-" + str(self.name)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        #do_something
        super().save(*args, **kwargs)  # Call the "real" save() method.
        #do_something