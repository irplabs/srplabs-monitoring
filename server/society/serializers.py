from rest_framework.serializers import ModelSerializer,PrimaryKeyRelatedField
from society.models import*
from utility.serializers import*
from rest_framework import*
from django.contrib.auth.models import Group,User

class SocietySerializer(ModelSerializer):

	organization = OrganizationSerializer(read_only=True)
	
	class Meta:
		model = Society
		fields = "__all__"	