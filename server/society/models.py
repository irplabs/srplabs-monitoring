# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from utility.models import *
from datetime import date
from decimal import Decimal

class Society(models.Model):

    # DATABASE FIELDS
    code = models.CharField(max_length=20,primary_key=True)
    organization = models.ForeignKey(Organization,on_delete=models.CASCADE,null=True)
    photograph = models.ImageField(upload_to = 'media/images/societies/',null=True)
    
    # META CLASS
    class Meta:
        verbose_name = 'Society'
        verbose_name_plural = 'Societies'

    # TO STRING METHOD
    def __str__(self):
        return str(self.code) + "-" + str(self.organization.name)

    # SAVE METHOD
    def save(self, *args, **kwargs):
        #do_something
        super().save(*args, **kwargs)  # Call the "real" save() method.
        #do_something
