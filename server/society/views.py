# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from django.contrib.auth.models import Group,User
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,RetrieveAPIView,CreateAPIView
from utility.models import*
from utility.serializers import*
from society.models import*
from society.serializers import*
from utility.authenticate import*
from server.settings import SECRET_KEY
import jwt


# Create your views here.

class SocietyListView(APIView):

	def get(self,request):

		access_group = ['ADMIN']
		if not isAuthenticated(request,access_group):
			return Response({'STATUS':'FORBIDDEN'})

		filters = {}

		societies = Society.objects.filter(**filters)
		serializer = SocietySerializer(societies,many=True)
		return Response({'status': 'SUCCESS', 'societies': serializer.data})